#!/bin/bash
echo ls

ls -al
echo set
set
apt-get update
apt-get install -y ssh

echo startuju dev
SERVER_NAME=bi-dev-tomas
SEVRER_HOST=$SERVER_NAME.aws.isrv.cz

ping -c 3 $SEVRER_HOST > /dev/null 2>&1
if [ $? -ne 0 ]
then
    echo $SEVRER_HOST is down 
    ssh root@jenkins-buildmaster.aws.isrv.cz "/centrum/builder/bin/aws-ec2-start-by-name.sh $SERVER_NAME"
fi
